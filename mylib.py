# Sends farewell... obviously
def greet():
    print "G'day!"

# Computes some VERY important things
def compute():
    a = 12
    b = 7
    print "Computing... %d * %d = %d" % (a, b, a*b)
